<?php
/**
 * Définit l'administration du plugin Pensebetes
 *
 * Installation et désinstallation du plugin
 *
 * @plugin     Pensebetes
 * @copyright  depuis 2019
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Pensebetes\Administrations
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Création et mise à jour du plugin Pensebetes
 *
 * @param  string $nom_meta_base_version version du schéma de données du plugin installé
 * @param  string $version_cible Version déclarée dans paquet.xml
 * @return void
**/

function pensebetes_upgrade($nom_meta_base_version, $version_cible){

	$maj = array();
	$maj['create'] = array(
		array('maj_tables', array('spip_pensebetes', 'spip_pensebetes_liens', 'spip_pensebetes_receveurs')),
		array('ecrire_config','pensebetes/mes_objets', array('spip_articles')),
		array('ecrire_config','pensebetes/mes_lieux', array('accueil')),
		array('ecrire_config','pensebetes/mes_boites', array('accueil','auteur')),
		array('ecrire_config','pensebetes/mes_statuts', array('0minirezo', '1comite')),
		array('ecrire_config','pensebetes/height', '12em'),
		array('ecrire_config','pensebetes/width', '12em'),
		array('ecrire_config','pensebetes/titre', '17'),
		array('ecrire_config','pensebetes/corps', '110'),
	);
	$maj['1.0.1'] = array(
		array('ecrire_config','pensebetes/mes_objets', array('article')),
		array('ecrire_config','pensebetes/mes_lieux', array('accueil')),
		array('ecrire_config','pensebetes/mes_boites', array('accueil','auteur')),
	);
	# récupéré et transformer les valeurs des anciennes configs
	$maj['1.0.2'] = array(
		array('cfg_1_0_2')
	);
	# ajouter des valeurs à la config
	$maj['1.0.3'] = array(
		array('ecrire_config','pensebetes/mes_statuts', array('0minirezo', '1comite'))
	);
	# transformer la table spip_pensebetes (poser un pense-bête sur les murs de plusieurs auteurs)
	$maj['1.0.4'] = array(
		array('maj_tables', array('spip_pensebetes_receveurs')),
		array('cfg_1_0_4'),
		array('sql_alter', 'TABLE spip_pensebetes DROP INDEX id_receveur'),
		array('sql_alter', 'TABLE spip_pensebetes DROP COLUMN id_receveur'),
	);
	# La taille du Pense-bête devient configurable
	$maj['1.0.5'] = array(
		array('ecrire_config','pensebetes/height', '12em'), // dans css/style_plugin_pensebetes.css.html
		array('ecrire_config','pensebetes/width', '12em'),
		array('ecrire_config','pensebetes/titre', '17'), // dans formulaires/editer_pensebete.php
		array('ecrire_config','pensebetes/corps', '110'),
	);
	
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Faire le ménage lors de la désinstallation du plugin Pensebetes
 *
 * @param  string $nom_meta_base_version version du schéma de données du plugin installé
 * @return void
**/

function pensebetes_vider_tables($nom_meta_base_version) {
	sql_drop_table("spip_pensebetes");
	sql_drop_table("spip_pensebetes_liens");
	sql_drop_table("spip_pensebetes_receveurs");
	effacer_config("pensebetes/mes_statuts");
	effacer_config("pensebetes/mes_objets");
	effacer_config("pensebetes/mes_lieux");
	effacer_config("pensebetes/mes_boites");
	effacer_config("pensebetes/espacepublic");
	effacer_config("pensebetes/listes");
	effacer_config("pensebetes/multiple");
	effacer_meta($nom_meta_base_version);
}

/**
 * Fonction privée : remplacer les noms d'objet par les noms de table
 *
 * Permettre de conserver la configuration définie par les versions précédentes à la version 1.0.2
 *
 * @return void
 */
function cfg_1_0_2() {
	include_spip('inc/config'); // pour la fonction lire_config()
	$mes_objets = lire_config("pensebetes/mes_objets");
	if ($mes_objets) {
		foreach($mes_objets as $cle => &$valeur){
			$table = table_objet($objet);
			if ($table) {
				$valeur = 'spip_' . $table;
			}
		}
	}
	ecrire_config("pensebetes/mes_objets", $mes_objets);
	
}

/**
 * Fonction privée : remplacer les valeurs d’id_objet à zéro
 *
 * Comptabilité ascendante
 * Permettre de conserver la configuration définie par les versions précédentes à la version 1.1.5
 *
 * @return void
 */
function cfg_1_0_4() {
	include_spip('inc/config'); // pour la fonction lire_config()
	# connaitre les statuts des auteurs qui peuvent échanger des pense-bêtes
	$mes_statuts = lire_config("pensebetes/mes_statuts",array());
	if ($mes_statuts) {
		// on récupère les id des auteurs des statuts
		$auteurs = array();
		$tableau = sql_allfetsel('id_auteur', 'spip_auteurs',  sql_in('statut', $mes_statuts));
		// transformation du format 0/id_auteur/1
		foreach ($tableau as $auteur) {
			$auteurs[] = $auteur['id_auteur'];
		}
		if ($auteurs) {
		# rechercher les pense-bêtes ayant un id_receveur à zéro
		// selection
			if ($resultats = sql_select('id_pensebete', 'spip_pensebetes','id_receveur="0"')) {
				// boucler sur les resultats
				while ($res = sql_fetch($resultats)) {
					// utiliser les resultats
					// $res['id_pensebete']
					foreach ($auteurs as $key => $id_auteur) {
						sql_insertq('spip_pensebetes_receveurs', 
							array(
								'id_receveur' => $id_auteur, 
								'id_pensebete' => $res['id_pensebete']
							)
						);
					}
				}
			}
		}
	}
	unset($tableau);// libérer la mémoire de l'opération précédente
	# activer la configuration pour que la gestion de listes soit possible
	ecrire_config('pensebetes/listes','on');
	# assurer la migration des données simples (pensebete à unique correspondant)
	if ($resultats = sql_select('id_pensebete,id_receveur', 'spip_pensebetes','id_receveur>0')) {
		// boucler sur les resultats
		while ($res = sql_fetch($resultats)) {
			// utiliser les resultats
			// $res['id_pensebete']
			sql_insertq('spip_pensebetes_receveurs', 
				array(
					'id_receveur' => $res['id_receveur'], 
					'id_pensebete' => $res['id_pensebete']
				)
			);
		}
	}
			
}