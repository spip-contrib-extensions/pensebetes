<?php
/**
 * Gestion de l'action de poser un pensebete
 *
 * @plugin Pense-bêtes
 * @copyright  2022
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package SPIP\Pensebetes\Actions
 */
 
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Action pour poser un Pense-bête depuis l'espace public
 *
 * dans l'environnement le titre et le message du pensebête sont transmis
 * mais l'émetteur et le recepteur sont dans les arguments sécurisés de l'action
 *
 * @uses API editer_pensebete
 * @link https://www.spip.net/fr_article5526.html
 *
 * @param  int    $id_pensebete    Identifiant de l'objet
 * @return void
**/
 
function action_poser_pensebete_dist(){

	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();

	list($id_auteur, $id_receveur) = explode('-', $arg);
	
	if ($id_auteur = intval($id_auteur) and $id_receveur = intval($id_receveur)){
		if ($titre = _request('titre') and $texte = _request('texte')){
			// s'assurer que les valeurs de l'environnement sont correctes
			include_spip('inc/config'); // pour la fonction lire_config()
			$maxlength_titre = lire_config("pensebetes/titre", 17);
			$maxlength_corps = lire_config("pensebetes/corps", 110);
			if (strlen($titre) > $maxlength_titre) $titre = substr($titre, 0, $maxlength_titre);
			if (strlen($texte) > $maxlength_corps) $texte = substr($titre, 0, $maxlength_corps);
			// créer l'objet
			include_spip('action/editer_pensebete');
			// mettre dans l'environnement le receveur qui sera placé dans la table auxiliaire
			set_request('auteurs_receveurs', array($id_receveur));
			// insérer et c'est magique, l'action editer_pensebete prend en charge la table auxiliaire
			$id_pensebete = pensebete_inserer();
				// Enregistre l'envoi dans la BD
				if ($id_pensebete > 0) {
					$err = pensebete_modifier($id_pensebete , [
						"id_pensebete"      => $id_pensebete,
						"id_donneur"        => $id_auteur,
						"date"              => date('Y-m-d H:i:s'),
						"titre"             => $titre,
						"texte"             => $texte,
						"maj"               => date('Y-m-d H:i:s')
						]
					);
				}
			spip_log("Le pense-bête n°$id_pensebete a été crée par l’auteur n°$id_auteur pour l’auteur $id_receveur.", 'pensebetes.' . _LOG_DEBUG);
			// Le pense-bête est-il a associer à un objet ?
			if ($associer_objet = _request('associer_objet')){
				spip_log("associer_objet = $associer_objet", 'pensebetes.' . _LOG_DEBUG);
				if (intval(_request('associer_objet'))) {
					// compat avec l'appel de la forme ajouter_id_article
					$objet = 'article';
					$id_objet = intval(_request('associer_objet'));
				} else {
					list($objet, $id_objet) = explode('|', _request('associer_objet'));
				}
				if ($objet and $id_objet) {
					include_spip('action/editer_liens');
					objet_associer( ['pensebete' => $id_pensebete], [$objet => $id_objet] );
					spip_log("Le pense-bête n°$id_pensebete a été associé à l'objet $objet n°$id_objet.", 'pensebetes.' . _LOG_DEBUG);
				}
			}
			if (isset($objet) and isset($id_objet)){
				// Invalider les caches (pour que la page soit recalculée et que le pense-bête disparaisse ou apparaisse).
				include_spip('inc/invalideur');
				suivre_invalideur("id='$objet/$id_objet'");
			}
		} else {
			spip_log('le formulaire ne transmet pas le titre et le texte nécessaire à la création du pense-bête ($titre = ' . $titre . ' $texte : ' . $texte . ').', 'pensebetes.' . _LOG_ERREUR);
		}
	} else {
		spip_log('Les arguments transmis ne sont pas exploitables ($arg = ' . $arg .').', 'pensebetes.' . _LOG_ERREUR);
	}
}