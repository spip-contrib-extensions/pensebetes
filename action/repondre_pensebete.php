<?php

/**
 * Gestion de l'action `repondre_pensebete`
 * 
 *
 * @plugin Pense-bêtes
 * @license GPL (c) 2019
 * @author Vincent CALLIES
 *
 * @package SPIP\Pensebetes\Actions
**/

// Sécurité
if (! defined('_ECRIRE_INC_VERSION')) return;

/**
 * Action pour répondre à une question dans un pense-bête
 * (posée via son modele modeles/pd_reponse.html)
 *
 * @param null|string $arg
 *     Identifiant du pense-bête et type du formulaire utilisé pour la réponse
 *     En absence utilise l'argument de l'action sécurisée.
 * @return array
 *     Liste (identifiant du pense-bête, Texte d'erreur éventuel)
 */
function action_repondre_pensebete_dist($arg = null) {
	// vérifier si c'est le bon auteur
	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	// vérification de l'argument (des variables transmises)
	[$id_donneur, $id_receveur, $formulaire] =  explode('-', $arg);
	if ($id_donneur = (int) $id_donneur 
		and $id_receveur = (int) $id_receveur
		and $formulaire = (string) $formulaire and strlen($formulaire)
	){

		// analyse des réponses selon le type du formulaire utilisé
		switch ($formulaire) {
			case 'coche':
				$r_cles = ['q', 'reponse', 'pas_fait'];
				break;
			case 'oui_non':
				$r_cles = ['q', 'reponse'];
				break;
			default:
				exit;
		}

		// création du champ `texte` du pense-bête de réponse
		$r_valeurs = [];
		foreach ($r_cles as $_cle) {
			$r_valeurs[$_cle] = _request($_cle);
		}
		$champs['texte'] = '';
		// la question dans le style gras
		if (!empty($r_valeurs['q'])){ 
			$champs['texte'] = '{{'.$r_valeurs['q']."}}\n";
		}
		if (empty($r_valeurs['reponse']) && isset($r_valeurs['pas_fait']) && strlen($r_valeurs['pas_fait']) ){
			$reponse = $r_valeurs['pas_fait'];
		} 
		if(!empty($r_valeurs['reponse'])){
			$reponse = $r_valeurs['reponse'];
		}
		// la réponse dans le style code (c'est une donnée)
		if (isset($reponse)){
			$champs['texte'] .= '<code>' . $reponse . '</code>';
		}

		// création des autres champs nécessaires à la réponse
		$champs['id_donneur'] = $id_donneur;
		$champs['date'] = date('Y-m-d H:i:s:');
		$champs['titre'] = "[réponse]";

		// appel de l'API « editer_objet »
		include_spip('action/editer_objet');
		// pas de réponse à un type d'auteur
		set_request('listes_receveurs','');
		// on répond à un auteur, celui qui veut la réponse (id_auteur_reponse dans le modele)
		set_request('auteurs_receveurs',[$id_receveur]);
		// la fonction retourne l’id de l’enregistrement ajouté en base, ou 0 en cas d’échec.
		if ($id_objet = objet_inserer('pensebete','', $champs)){
			// il faut alors s'occuper de la table auxiliaire
			// la fonction retourne une chaîne vide si tout s’est bien passé ou l’erreur en cas d’échec.
			if ($erreur = objet_modifier('pensebete', $id_objet, '')){
				spip_log($champs, 'pensebetes.' . _LOG_ERREUR);
			} else {
				// la réponse a été envoyée avec succès.
				include_spip("inc/filtres");
				$bouton_ok = _T('pensebete:modele_reponse_bnt');
				$corps = '<p>' . _T('pensebete:modele_reponse_succes') . '</p>';
				if ($redirect = _request('redirect')) {
					$corps .= "<div style='text-align:center;'><a href='$redirect' style='color: white;
					background: #DB1762;border-radius: 3px;padding:6px;font-weight:bold;'>$bouton_ok</a></div>";
				}
				$titre = _T('pensebete:modele_reponse_succes_titre');
				include_spip('inc/minipres');
				echo minipres($titre, $corps);
				exit;
			}
		} else {
			spip_log($champs, 'pensebetes.' . _LOG_ERREUR);
		}
	} else {
		spip_log("L'arg $arg n'est pas valable", 'pensebetes.' . _LOG_ERREUR);
	}
}