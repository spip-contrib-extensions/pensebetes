<?php
/**
 * Créer et envoyer un pensebete,
 * même sans être identifié.
 *
 * Cette fonction peut permettre au système d'envoyer des notifications par pensebetes
 * lors d'évènements.
 *
 *   exemple d'utilisation avec fonction charger_fonction()
 *     ```
 *     $envoyer_pensebete = charger_fonction('envoyer_pensebete', 'inc');
 *     $envoyer_pensebete(1,['auteurs_receveurs' => 1],'Notification','Un auteur devient administrateur !','auteur|3');
 *     ```
 */

// Sécurité : vérifier un appel depuis SPIP
if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Permettre de créer et d'envoyer un pense-bête
 *
 * @param  int    $id_donneur      auteur émetteur
 * @param  array  $receveurs       auteurs bénéficaires
 *                                 deux clés : `auteurs_receveurs`, la liste d'id d'auteurs
 *                                             `listes_receveurs`, la liste des statuts
 * @param  string $titre           intitulé du pense-bête
 * @param  string $texte           message du pense-bête
 * @param  string $associer_objet  objet auquel est éventuellement associé le pense-bête
 * @return array  $retours         tableau
 *                                 deux clés : `ok` boolean pour le succès 
 *                                             `message` string pour l'explication
 */
function inc_envoyer_pensebete_dist(int $id_donneur, array $receveurs, string $titre, string $texte, ?string $associer_objet=''){
	$retours = [
		'ok'   => false,
		'message' => ''
	];

	// 1°) vérifier et établir les éléments pour créer le pensebete

	// les champs de la table principale du pensebete
	$champs = [
		'id_pensebete'      => '',
		'id_donneur'        => $id_donneur,
		'date'              => date('Y-m-d H:i:s'),
		'titre'             => $titre,
		'texte'             => $texte,
		'maj'               => date('Y-m-d H:i:s'),
	];
	// -- le cas d'auteurs receveurs identifiés par leur identifiant unique
	if (isset($receveurs['auteurs_receveurs'])){
		$ids = [];
		// permettre de transmettre juste un int ou un tableau de int
		if (is_array($receveurs['auteurs_receveurs'])) {
			$ids = $receveurs['auteurs_receveurs'];
			// vérifier que les valeurs du tableau sont des integer
			$auteurs_receveurs = array_filter($ids, function($v, $k) {
			    return $v == (int) $v;
			}, ARRAY_FILTER_USE_BOTH);
		} elseif ($ids = (int) $receveurs['auteurs_receveurs']) {
			$ids = [$ids];
		}
	}
	// -- le cas d’auteurs receveurs identifiés par leur statut
	if (isset($receveurs['listes_receveurs']) and is_array($receveurs['listes_receveurs'])){
		$statuts = $receveurs['listes_receveurs'];
		// vérifier que les valeurs correspondent bien aux statuts des auteurs
	}
	// -- s'il n'y a pas d'auteurs receveurs
	if (empty($ids) and empty($statuts)){
		$retours['message'] .= 'Absence d’auteurs receveurs';
	} else {

	// 2°) créer le pense-bete
	//     en trois étapes :
	//     -- une création indiquant le contenu et l'émetteur
	//     -- une modification indiquant les receveurs
	//     -- une liaison si nécessaire à un objet

		include_spip('inc/autoriser');
		include_spip('action/editer_objet');
		// donner une autorisation exceptionnelle temporaire pour créer un pensebete
		autoriser_exception('creer', 'pensebete');
		if ($id_objet = objet_inserer('pensebete', '', $champs)){
			$retours['ok'] = true;
			$retours['message'] .= "Le pense-bête n°$id_objet a été créé.";
			autoriser_exception('creer', 'pensebete', $id_objet, false);
			// Modifier le pensebete pour que l'API place
			// le(s) receveur(s) dans sa table auxiliaire
			// Pour cela les mettre dans l'environnement.
			if (!empty($ids)){
				set_request('auteurs_receveurs', $ids);
			}
			if (!empty($statuts)){
				set_request('listes_receveurs',$statuts);
			}
			// donner une autorisation exceptionnelle temporaire pour modifier un pensebete
			autoriser_exception('modifier', 'pensebete', $id_objet);
			if ($erreur = objet_modifier('pensebete', $id_objet,[])){
				$retours['ok'] = false;
				$retours['message'] .= $erreur;
			} else {
				$retours['message'] .= '<br>' . _T('pensebete:texte_donneur',['id' => $id_donneur]);
				if (!empty($ids) && ($nb = count($ids))) {
					$ids = implode(',', $ids);
					if ($nb === 1) {
						$retours['message'] .= '<br>' . _T('pensebete:texte_receveur_id', ['id' => $ids]);
					} else {
						$retours['message'] .= '<br>' . _T('pensebete:texte_receveurs_ids', ['nb' => $nb, 'id' => $ids]);
					}
				}
				if (!empty($statuts) && ($nb = count($statuts))) {
					$statuts = implode(',', $statuts);
					if ($nb === 1) {
						$retours['message'] .= '<br>' . _T('pensebete:texte_receveur_statut', ['st' => $statuts]);
					} else {
						$retours['message'] .= '<br>' . _T('pensebete:texte_receveur_statut', ['st' => $statuts]);
					}
				}
				$retours['id_pensebete'] = $id_objet;
				// Le pense-bête a été créé et modifié avec succès ;
				// on se préoccupe d’associer le pensebete à un objet.
				if ($associer_objet) {
					if (intval($associer_objet)) {
						// compat avec l'appel de la forme ajouter_id_article
						$objet_associe = 'article';
						$id_objet_associe = intval($associer_objet);
					} else {
						// présentation orthodoxe objet|id_objet
						list($objet_associe, $id_objet_associe) = explode('|', $associer_objet);
					}
					if ($objet_associe && $id_objet_associe) {
						include_spip('action/editer_liens');
						objet_associer(['pensebete' => $id_objet], [$objet_associe => $id_objet_associe]);
						$retours['message'] .= '<br>' . _T('pensebete:texte_objet_associe', ['objet_associe' => $objet_associe, 'id' => $id_objet_associe]);
					} else {
						$retours['message'] = _T('pensebete:erreur_association');
					}
				}
			}
			autoriser_exception('modifier', 'pensebete', $id_objet, false);
		} else {
			autoriser_exception('creer', 'pensebete', $id_objet, false);
			$retours['message'] = _T('pensebete:texte_pble_creation');
		}
	}
	return $retours;
}