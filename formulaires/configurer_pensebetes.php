<?php
if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Un simple formulaire de config,
 * on a juste à déclarer les saisies
 **/
function formulaires_configurer_pensebetes_saisies_dist(){
	# $saisies est un tableau décrivant les saisies à afficher dans le formulaire de configuration

	# limitation par statut des éditeurs pouvant échanger des penses-bêtes
	$statuts = objet_info('spip_auteurs','statut_titres');
	unset($statuts[0], $statuts['5poubelle']);
	foreach ($statuts as $cle => &$valeur){
		$idiome = _T($valeur, [], ['force'=>'']);
		if ($idiome){
			$valeur = $idiome;
		} else {
			$valeur = $cle;
		}
	}

	$saisies = [
		[ // le fieldset sur les liaisons
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'liaisons_dangereuses',
				'label' => '<:espace_prive:>',
			],
			'saisies' => [ // les champs dans le fieldset
				[
					'saisie' => 'checkbox',
					'options' => [
						'nom' => 'mes_statuts',
						'label' => '<:pensebete:cfg_statuts:>',
						'explication' => '<:pensebete:cfg_statuts_explication:>',
						'data' => $statuts,
						'obligatoire' => 'oui',
					],
				],
				[
					'saisie' => 'case',
					'options' => [
						'nom' => 'multiple',
						'label' => '<:pensebete:cfg_multiple:>',
						'explication' => '<:pensebete:cfg_multiple_explication:>',
					],
				],
				[
					'saisie' => 'case',
					'options' => [
						'nom' => 'listes',
						'label' => '<:pensebete:cfg_listes:>',
						'explication' => '<:pensebete:cfg_listes_explication:>',
						'afficher_si' => '@multiple@=="on"'
					],
				],
				[
					'saisie' => 'choisir_objets',
					'options' => [
						'nom' => 'mes_objets',
						'label' => '<:pensebete:cfg_objets:>',
						'explication' => '<:pensebete:cfg_explication_espace_privee:>',
						'exclus' => [
							'spip_pensebetes'
						],
					],
				],
				[
					'saisie' => 'checkbox',
					'options' => [
						'nom' => 'mes_lieux',
						'label' => '<:pensebete:cfg_lieux:>',
						'explication' => '<:pensebete:cfg_lieux_explication:>',
						'data' => [
							'accueil' => '<:icone_accueil:>'
						],
					],
				],
				[
					'saisie' => 'checkbox',
					'options' => [
						'nom' => 'mes_boites',
						'label' => '<:pensebete:cfg_boites:>',
						'explication' => '<:pensebete:cfg_boites_explication:>',
						'data' => [
							'accueil' => '<:icone_accueil:>',
							'auteur' => '<:info_auteurs:>'
						],
					],
				],
			],
		],
		[ // le fieldset concernant la CSS dans l'espace public
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'cssoupas',
				'label' => '<:pensebete:cfg_public:>',
			],
			'saisies' => [ // les champs dans le fieldset
				[
					'saisie' => 'case',
					'options' => [
						'nom' => 'espacepublic',
						'explication' => '<:pensebete:cfg_explication_espace_public:>',
						'label_case' => '<:pensebete:cfg_espace_public:>',
						'conteneur_class' => 'pleine_largeur'
					],
				],
			],
		],		
		[ // le fieldset sur la taille des pense-bête
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'la_taille_compte',
				'label' => '<:pensebete:cfg_taille:>',
			],
			'saisies' => [ // les champs dans le fieldset
				[
					'saisie' => 'explication',
					'options' => [
						'nom' => 'la_class',
						'titre' => '<:pensebete:cfg_la_class:>',
						'texte' => '<:pensebete:cfg_la_class_explication:>',
						'conteneur_class' => 'pleine_largeur'
					],
				],
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'height',
						'label' => '<:pensebete:cfg_height:>',
					],
					'verifier' => [
						'type' => 'regex',
						'options' => ['modele' => '@[0-9]+\.?[0-9]+(?:px|em|ex|%|in|cn|mm|pt|pc)@'],
					],
				],
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'width',
						'label' => '<:pensebete:cfg_width:>',
					],
					'verifier' => [
						'type' => 'regex',
						'options' => array('modele' => '@[0-9]+\.?[0-9]+(?:px|em|ex|%|in|cn|mm|pt|pc)@'),
					],
				],
				[
					'saisie' => 'explication',
					'options' => [
						'nom' => 'les_caracteres',
						'titre' => '<:pensebete:cfg_les_caracteres:>',
						'texte' => '<:pensebete:cfg_les_caracteres_explication:>',
						'conteneur_class' => 'pleine_largeur'
					],
				],
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'titre',
						'label' => '<:pensebete:cfg_titre:>',
					],
					'verifier' => [
						'type' => 'entier',
						'options' => ['min' => 1, 'max' => 255],
					],
				],
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'corps',
						'label' => '<:pensebete:cfg_corps:>',
					],
					'verifier' => [
						'type' => 'entier',
						'options' => ['min' => 1, 'max' => 65535],
					],
				],
			],
		],
	];
	return $saisies;
}