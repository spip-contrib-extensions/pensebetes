<?php
/**
 * Gestion du formulaire de d'édition de pensebete
 *
 * @plugin Pensebetes
 * @copyright  2020-2021
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package SPIP\Pensebetes\Formulaires
 */

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Chargement des valeurs de SAISIES
 *
 * Aide à la création du formulaire avec le plugin SAISIES
 *
 * Les valeurs sont calculées en fonction de la configuration
 * mais aussi d'éventuelles autorisations qui permettent de gérer plus finement
 * la configuration.
 *
 *
 */
function formulaires_editer_pensebete_saisies_dist($id_pensebete='new', $id_parent='', $retour='', $associer_objet='', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){
	include_spip('inc/config'); // pour la fonction lire_config()
	$maxlength_titre = lire_config("pensebetes/titre", 17);
	$maxlength_corps = lire_config("pensebetes/corps", 110);
	$id_pensebete = (int) $id_pensebete;

	# A-t-on demandé à ce que le pense-bête soit associé à un objet éditorial ?
	if ($associer_objet) {
		list($objet, ) = explode('|', $associer_objet);
		$icone_objet = chemin_image($objet);
		$c_associe = 'oui';
	} else {
		$c_associe = 'non';
		$icone_objet= '';
	}

	# entre quels types d'auteurs peuvent s'échanger des Penses-bêtes ?
	$mes_statuts = lire_config("pensebetes/mes_statuts", ['0minirezo', '1comite']);

	$total = '';

	include_spip('inc/autoriser');
	// choisir les auteurs pour envoyer un message
	// -- en les sélectionnant en fonction de leur statut
	//    si cela est autorisé.
	if (autoriser('creer','_pensebete_par_statut')) {
		// avoir tous les statuts possibles d'un auteur
		$liste_possible = objet_info('spip_auteurs','statut_titres');
		$listes = [];
		foreach ($liste_possible as $cle => $idiome){
			// limiter la liste des statuts à ceux prévus dans la configuration du plugin
			if (in_array($cle, $mes_statuts)){
				$listes[$cle] =  _T($idiome);
			}
		}
		$listes_receveurs = [
			'saisie' => 'checkbox',
			'options' => [
				'nom' => 'listes_receveurs',
				//'label' => _T('pensebete:label_listes'),
				'obligatoire' => 'non',
				'multiple' => 'oui',
				'data' => $listes,
				'afficher_si' => '@choix@ == "categorie"'
				]
			];
		$data['categorie'] = _T('pensebete:selecteur_categorie');
	} else {
		$listes_receveurs = [
			'saisie' => 'hidden',
			'options' => [
				'nom' => 'listes_receveurs',
				'defaut' => ''
			]
		];
	}

	// choisir les auteurs pour envoyer un message
	// -- en les sélectionnant individuellement, en fonction de leur id
	//    avec une saisie multiple, si elle est autorisée.
	$auteurs_receveurs = [
		'saisie' => 'auteurs',
		'options' => [
			'nom' => 'auteurs_receveurs',
			'label' => _T('pensebete:label_receveur'),
			// proposer une selection multiple d'auteur
			// si la configuration le permet
			'multiple' => autoriser('creer','_pensebete_multi_auteurs') ? 'oui' : '',
			'class' => 'select2',
			'option_intro' => '<:pensebete:texte_option_intro_receveur:>',
			'obligatoire' =>  empty($listes) ? 'oui' : '',
			'statut' => $mes_statuts,
			'afficher_si' => '@choix@ == "auteurs"'
		]
	];
	if (autoriser('creer','_pensebete_multi_auteurs')){
		$data['auteurs'] = _T('pensebete:selecteur_auteurs');
	} else {
		$data['auteurs'] = _T('pensebete:selecteur_auteur');
	}
		
	// placer un selecteur
	// - envoyer à une catégorie d'auteur (si c'est autorisé)
	// - envoyer à un auteur (et même de multiples auteurs si c'est autorisé)
	// - envoyer à moi-même
	$data['moi'] = _T('pensebete:selecteur_moi');
	$selecteur = [ // champ titre : ligne de texte
			'saisie' => 'radio',
			'options' => [
				'nom' => 'choix',
				'label' => 'Le pense-bête est pour :',
				'defaut' => 'auteurs',
				'data' => $data
			],
		];

	# définir les saisies dans deux ou trois fieldsets, selon l'association ou non d'un objet
	$saisies = [
		[ // le fieldset 
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'le_pensebete',
				'label' => _T('pensebete:fieldset_destinataire'),
				'icone' => 'pensebete-24'
			],
			'saisies' => [ // les champs dans le fieldset 
				[ // champ retour nécessaire notamment à la création pour aller directement au contenu
					'saisie' => 'hidden',
					'options' => [
						'nom' => 'retour'
					]
				],
				[ // champ id_donneur (numéro identification auteur du pense-bête)
					'saisie' => 'hidden',
					'options' => [
						'nom' => 'id_donneur',
						'defaut' => 0
					]
				],
				[ // champ date (date de création du pense-bête)
					'saisie' => 'hidden',
					'options' => [
						'nom' => 'date',
					]
				],
				[ // champ associer_objet (A-t-on demandé que ce pense-bête soit associé à un objet )
					'saisie' => 'hidden',
					'options' => [
						'nom' => 'associer_objet',
						'defaut' => $associer_objet
					]
				], $selecteur, 
				$auteurs_receveurs,
				$listes_receveurs,
			],
		],
		[ // le fieldset 
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'le_pensebete',
				'label' => _T('pensebete:info_le_pensebete'),
				'icone' => 'pensebete-24'
			],
			'saisies' => [ // les champs dans le fieldset 
				[ // champ titre : ligne de texte
					'saisie' => 'input',
					'options' => [
						'nom' => 'titre',
						'label' => _T('pensebete:label_titre'),
						'maxlength' => $maxlength_titre, // limiter la taille du titre
						'obligatoire' => 'oui',
					],
					'verifier' => [
						'type' => 'taille',
						'options' => array ('min' => 0, 'max' => $maxlength_titre),
					]
				],
				[ // champ texte : un bloc de texte
					'saisie' => 'textarea',
					'options' => [
						'nom' => 'texte',
						'label' => _T('pensebete:label_message'),
						'rows' => 3,
						'cols' => 60,
						'longueur_max' => $maxlength_corps, // limiter la taille du texte
						'aide' => 'textepb'
					],
					'verifier' => [
						'type' => 'taille',
						'options' => array ('min' => 0, 'max' => $maxlength_corps)
					],
				],
			],
		]
	];
	if ($associer_objet) {
		$saisies[] = [ // le fieldset
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'lassociation',
				'label' => _T ('pensebete:info_lassociation'),
				'icone' => $icone_objet,
			],
			'saisies' => [ // les champs dans le second fieldset
				[ // hors fieldset : association
					'saisie' => 'oui_non',
					'options' => [
						'nom' => 'c_associe',
						'label' => _T ('pensebete:texte_associer_pensebete'),
						'valeur_oui' => 'oui',
						'valeur_non' => 'non',
						'defaut' => $c_associe
					]
				],
			]
		];
	}

	return $saisies;
}


/**
 * Chargement du formulaire d'édition de pensebete
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @param int|string $id_pensebete identifiant de l'objet ou 'new' si absent.
 * @param int $id_parent identifiant de la rubrique.
 * @param string $retour lien pour le retour.
 * @param string $associer_objet
 *     Éventuel 'objet|x' indiquant de lier le mot créé à cet objet,
 *     tel que 'article|3'
 * @param int $lier_trad 
 * @param string $config_fonc 
 * @param array $row 
 * @param array $hidden 
 * @uses formulaires_editer_objet_charger()
 * @uses mes_saisies_pensebete()
 *
 */
function formulaires_editer_pensebete_charger_dist($id_pensebete='new', $id_parent='', $retour='', $associer_objet='', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){
	$valeurs = formulaires_editer_objet_charger('pensebete',$id_pensebete,$id_parent,$lier_trad,$retour,$config_fonc,$row,$hidden);
	# si c'est une création, présenter au mieux
	if (!intval($id_pensebete)){
		# donner la date d'aujourd'hui
		$valeurs['date'] = date('Y-m-d H:i:s');
		# s'il n'y a pas d'id_donneur, donner l'id de l'auteur
		if (!_request('id_donneur')) {
			$valeurs['id_donneur'] = $GLOBALS['visiteur_session']['id_auteur'] ?? 0;
		}
		# Si titre dans l'url : fixer le titre (à 17 caractères)
		if ($titre = (string) _request('titre')) {
			$valeurs['titre'] = substr ($titre, 0, 16);
		}
	} else {
	# si c'est une modification, présenter les auteurs destinataires
		// selon que la selection est multiple ou simple
		// la valeur par défaut n'aura pas le même type
		if (autoriser('creer','_pensebete_multi_auteurs')){
			$tableau = sql_allfetsel('id_receveur', 'spip_pensebetes_receveurs',  'id_pensebete=' . intval($id_pensebete));
			$auteurs = [];
			foreach ($tableau as $ligne) {
				$auteurs[] = $ligne['id_receveur'];
			}
			$valeurs['auteurs_receveurs'] = $auteurs;
		} else {
			$valeurs['auteurs_receveurs'] = sql_getfetsel('id_receveur', 'spip_pensebetes_receveurs',  'id_pensebete=' . intval($id_pensebete));
		}
	}
	// nécessaire unset en cas d'appel en modification en cliquant sur l'id depuis une liste en ajax 
	// et avec un retour en erreur lors de la tentative de modification (cas très particulier).
	unset ($valeurs['id_pensebete']);
	return $valeurs;
}

/**
 * Identifier le formulaire en faisant abstraction des parametres qui ne representent pas l'objet edite
 *
 * @param int|string $id_pensebete identifiant de l'objet ou 'new' si absent.
 * @param int $id_parent identifiant de la rubrique.
 * @param string $retour lien pour le retour.
 * @param string $associer_objet
 *     Éventuel 'objet|x' indiquant de lier le mot créé à cet objet,
 *     tel que 'article|3'
 * @param int $lier_trad 
 * @param string $config_fonc 
 * @param array $row 
 * @param array $hidden 
 */
function formulaires_editer_pensebete_identifier_dist($id_pensebete='new', $id_parent='', $retour='', $associer_objet='', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){
	return serialize([intval($id_pensebete), $associer_objet]);
}

/**
 * Vérification du formulaire d'édition de pensebete
 *
 * @param int|string $id_pensebete identifiant de l'objet ou 'new' si absent.
 * @param int $id_parent identifiant de la rubrique.
 * @param string $retour lien pour le retour.
 * @param string $associer_objet
 *     Éventuel 'objet|x' indiquant de lier le mot créé à cet objet,
 *     tel que 'article|3'
 * @param int $lier_trad 
 * @param string $config_fonc 
 * @param array $row 
 * @param array $hidden 
 *
 */
function formulaires_editer_pensebete_verifier_dist($id_pensebete='new', $id_parent='', $retour='', $associer_objet='', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){
	$erreurs = [];//formulaires_editer_objet_verifier('pensebete',$id_pensebete);
	// Il faut au moins un auteur ou une liste
	$auteurs_receveurs = _request('auteurs_receveurs');
	$listes_receveurs = _request('listes_receveurs');
	// à moins que l'on écrive à soi-même
	if (
		!(_request('choix') === 'moi' && isset($GLOBALS['visiteur_session']['id_auteur'])) 
		&& empty($listes_receveurs)
	){
		if (
			// le cas de la selection de l'option_intro qui n'est pas une selection
			(is_array($auteurs_receveurs) && isset($auteurs_receveurs[0]) && empty($auteurs_receveurs[0]))
			|| 
			(empty($auteurs_receveurs)) 
		) {
			$erreurs['message_erreur'] = _T('pensebete:saisies_obligatoire_receveur');
		}
	}
	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de pensebete
 *
 * Le traitement effectue une mise à zéro de l'id_auteur 
 * pour éviter des associations considérées comme inutiles.
 *
 * @param int|string $id_pensebete identifiant de l'objet ou 'new' si absent.
 * @param int $id_parent identifiant de la rubrique.
 * @param string $retour lien pour le retour.
 * @param string $associer_objet
 *     Éventuel 'objet|x' indiquant de lier le mot créé à cet objet,
 *     tel que 'article|3'
 * @param int $lier_trad 
 * @param string $config_fonc 
 * @param array $row 
 * @param array $hidden 
 * @uses objet_inserer()
 * @uses objet_associer()
 *
 */
function formulaires_editer_pensebete_traiter_dist($id_pensebete='new', $id_parent='', $retour='', $associer_objet='', $lier_trad=0, $config_fonc='', $row=[], $hidden=''){
	// le cas où le destinataire est l'auteur saisissant le pense-bête
	if (_request('choix') === 'moi'){
		set_request('auteurs_receveurs',[0 => $GLOBALS['visiteur_session']['id_auteur']]);
	}

	$retours = [];
	// éviter que l'auteur soit associé au pense-bête.
	set_request('id_auteur','');

	// rompre l'association si nécessaire		
	if (_request('c_associe') == 'non'){
		set_request('associer_objet','');
	} else { // on garde la donnée de redirection s'il y a une association
		$redirect = _request('redirect');
	}

	// on traite les données
	$retours = formulaires_editer_objet_traiter('pensebete', $id_pensebete, $id_parent, $lier_trad, $retour, $config_fonc, $row, $hidden);
	
	// si le pense-bête a été créé ou modifié avec succès, on se préoccupe d’associer le pensebete à un objet
	if (_request('associer_objet') and ($id_pensebete = $retours['id_pensebete'])) {
		if (intval(_request('associer_objet'))) {
			// compat avec l'appel de la forme ajouter_id_article
			$objet = 'article';
			$id_objet = intval(_request('associer_objet'));
		} else {
			list($objet, $id_objet) = explode('|', _request('associer_objet'));
		}
		if ($objet && $id_objet && autoriser('modifier', $objet, $id_objet)) {
			include_spip('action/editer_liens');
			objet_associer( array('pensebete' => $id_pensebete), array($objet => $id_objet) );
			// rediriger vers l'objet
			if (isset($redirect)) {
				$retours['redirect'] = parametre_url($redirect, 'id_lien_ajoute', $id_pensebete, '&');
			}
			return $retours;
		} else {
			return array('message_erreur'=>_T('pensebete:erreur_association'));
		}
	} // si remord par rapport à l'association à l'objet, rediriger vers le pense-bête
	elseif (_request('c_associe')=='non') {
		$retours['redirect'] = generer_url_ecrire ('pensebete', 'id_pensebete=' . $retours['id_pensebete']);
	}
	return $retours;	
}