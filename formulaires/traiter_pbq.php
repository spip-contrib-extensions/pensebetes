<?php
/**
 * Petite fonction pour avoir l'id de l'auteur connecté
 *
 * la fonction est utilisée par le MODELE pbq
 *
 * @plugin     Pensebetes
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Pensebetes\Formulaires
 */

if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Traitement du formulaire d'édition
 *
 * Le traitement est nécessaire pour placer l'identifiant unique
 * de l'auteur à qui la réponse sera apportée dans la variable id_rep.
 *
 * @uses plugin inserer_modele
 *
 * @param  array  $champs Champs du formulaire du plugin inserer_modele
 * @return string $code   Arguments en ligne pour appeler le modèle
 */
function formulaires_traiter_pbq_dist($champs){
	$code = '<' . _request('modele') . '|id_rep=' . $GLOBALS['visiteur_session']['id_auteur'];
	foreach ($champs as $champ) {
		if ($champ != 'modele' && _request($champ) && _request($champ) != '') {
			if ($champ == _request($champ)) {
				$code .= "|$champ";
			} elseif (is_array(_request($champ))) {
				// On transforme les tableaux en une liste
				$code .= "|$champ=" . implode(',', _request($champ));
			} else {
				$code .= "|$champ=" . _request($champ);
			}
		}
	}
	$code .= '>';

	return $code;
}