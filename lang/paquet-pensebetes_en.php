<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/pensebetes-paquet-xml-pensebetes?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// P
	'pensebetes_description' => 'A plugin designed to remember what you plan to do or to remind someone what to do: wall sticker for the private part of SPIP.',
	'pensebetes_nom' => 'Pense-bêtes',
	'pensebetes_slogan' => 'A sticky plugin !',
];
