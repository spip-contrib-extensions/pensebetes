<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/pensebetes.git

return [

	// C
	'cfg_boites' => 'Boîtes d’information',
	'cfg_boites_explication' => 'Boites d’activité relatives aux pense-bêtes envoyés ou reçus s’affichant lattéralement sur :',
	'cfg_corps' => 'Nombre de caractères pour le texte',
	'cfg_espace_public' => 'Feuille de style dans la partie publique',
	'cfg_explication_espace_privee' => 'Le pense-bête y est un moyen de communication entre les autrices et auteurs, invisible de la partie publique. Les pense-bêtes apparaissent sur le mur de l’autrice ou de auteur, mais également sur les objets auxquels ils seront liés. Indiquez ci-après les liaisons possible',
	'cfg_explication_espace_public' => 'L’installation de la feuille de style privée du plugin dans la partie publique vous permet de bénéficier de l’apparence du Pense-bête tel que vous la voyez dans la partie privée (Il ne s’agit que d’une option. Votre thème pour l’espace public pourrait prévoir une autre apparence.).',
	'cfg_height' => 'Hauteur du pense-bête',
	'cfg_la_class' => 'Définir la hauteur et la largeur du pense-bête',
	'cfg_la_class_explication' => 'La taille compte, dit-on. Nous vous laissons donc définir la hauteur et largeur du Pense-bête (les valeurs par défaut à l’installation sont 12 et 12)',
	'cfg_les_caracteres' => 'Définir le nombre de caractères du pense-bête',
	'cfg_les_caracteres_explication' => 'D’autres disent que c’est le caractère qui compte. Nous vous laissons donc définir le nombre de caractère du Pense-bête (les valeurs par défaut est de 17 pour le titre et 110 pour le corps).',
	'cfg_lieux' => 'Pense-bêtes dans (lieux) :',
	'cfg_lieux_explication' => 'Afficher les pense-bêtes de l’autrice ou l’auteur sur les pages :',
	'cfg_listes' => 'Envoi par catégorie',
	'cfg_listes_explication' => 'Permettre d’envoyer un pense-bête à une catégorie d’auteurs (selon le statut)',
	'cfg_multiple' => 'Envoi à plusieurs auteurs',
	'cfg_multiple_explication' => 'Permettre de poser un même pense-bête sur les murs de plusieurs auteurs',
	'cfg_objets' => 'Pense-bêtes sur (objets) :',
	'cfg_public' => 'Espace public',
	'cfg_statuts' => 'Statuts des autrices et des auteurs pouvant échanger des Pense-bêtes',
	'cfg_statuts_explication' => 'Attention si vous valider les visiteurs (6forum), car cela demande une prise en charge des penses-bêtes dans l’espace public',
	'cfg_taille' => 'La taille compte...',
	'cfg_titre' => 'Nombre de caractères pour le titre',
	'cfg_width' => 'Largeur du pense-bête',

	// D
	'demo_espacepublic_off' => 'Attention, vous devez configurer le plugin pour inclure la feuille de style dans l’espace public et voir les pense-bêtes mis en forme.',
	'demo_titre' => 'Démonstration de Pense-bêtes',

	// E
	'erreur_association' => 'Le pense-bête est créé (n° @id_pensebete@) mais l’association à l’objet éditorial n’a pas pu être faite.',
	'erreur_suppression' => 'Vous n’avez pas l’autorisation de supprimer ce pense-bête',

	// F
	'fieldset_destinataire' => 'Destinataire',

	// I
	'icone_creer_pensebete' => 'Créer un pense-bête',
	'icone_modifier_pensebete' => 'Modifier ce pense-bête',
	'info_1_pensebete' => '1 pense-bête',
	'info_1_pensebete_donne' => '1 pense-bête donné',
	'info_1_pensebete_recu' => '1 pense-bête reçu',
	'info_aucun_pensebete' => 'Aucun pense-bête',
	'info_aucun_pensebete_donne' => 'Aucun pense-bête donné',
	'info_aucun_pensebete_recu' => 'Aucun pense-bête reçu',
	'info_lassociation' => 'L’association',
	'info_le_pensebete' => 'Le pense-bête',
	'info_nb_a_moi' => 'dont @nb@ à moi',
	'info_nb_par_moi' => 'dont @nb@ par moi',
	'info_nb_pensebetes' => '@nb@ pense-bêtes',
	'info_nb_pensebetes_donnes' => '@nb@ pense-bêtes donnés',
	'info_nb_pensebetes_recus' => '@nb@ pense-bêtes reçus',

	// L
	'label_a' => 'à',
	'label_de' => 'De',
	'label_donneur' => 'De',
	'label_listes' => 'Listes',
	'label_message' => 'Message',
	'label_receveur' => 'Pour',
	'label_titre' => 'Titre',
	'lien_retirer_pensebete' => 'Retirer ce pense-bête',
	'log_action_supprimer_pensebete' => 'action_supprimer_pensebete_dist : supression par l’auteur n°@aut@ du pense-bête n°@id@ de la table ’spip_pensebetes’.',
	'log_action_supprimer_pensebete_imp' => 'action_supprimer_pensebete_dist : la supression par l’auteur n°@aut@ du pense-bête n°@id@ de la table ’spip_pensebetes’ s’est avérée impossible.',
	'log_action_supprimer_receveur' => 'action_supprimer_pensebete_dist : supression par l’auteur n°@aut@ de @nb@ entrée(s) dans la table ’spip_pensebetes_receveurs’ pour le pense-bête n°@id@.',
	'log_action_supprimer_receveur_imp' => 'action_supprimer_pensebete_dist : la supression par l’auteur n°@aut@ d’enregistrement la table ’spip_pensebetes_receveurs’ pour le pense-bête n°@id@ s’est avérée impossible.',

	// M
	'modele_bouton' => 'Bouton',
	'modele_btn_texte' => 'Envoyer la réponse',
	'modele_cocher' => 'Cocher',
	'modele_explication_q' => 'Question posée (reçue lors de la réponse).',
	'modele_explication_q_cocher' => 'Coche cette case si c’est fait',
	'modele_explication_q_oui_non' => 'Est-ce que c’est oui ou bien c’est non ?',
	'modele_fait' => 'Fait',
	'modele_label_non' => 'Non',
	'modele_label_oui' => 'Oui',
	'modele_oui_non' => 'oui ou non',
	'modele_pas_fait' => 'Pas fait',
	'modele_question' => 'Question',
	'modele_reponse_bnt' => 'Ok',
	'modele_reponse_succes' => 'Votre réponse a été envoyée avec succès !',
	'modele_reponse_succes_titre' => 'Succès',

	// S
	'saisies_obligatoire_receveur' => 'Le pense-bête doit obligatoirement avoir un destinataire.',
	'selecteur_auteur' => 'un destinataire nommément désigné',
	'selecteur_auteurs' => 'un ou plusieurs destinataires nommément désignés',
	'selecteur_categorie' => 'une catégorie de destinataires',
	'selecteur_moi' => 'moi',

	// T
	'texte_ajouter_pensebete' => 'Ajouter un pense-bête',
	'texte_association' => 'Association',
	'texte_associer_pensebete' => 'Voulez vous associer ce pense-bête à cet objet ?',
	'texte_avertissement_retrait' => 'Êtes-vous sûr de vouloir supprimer ce pense-bête ?',
	'texte_creer_associer_pensebete' => 'Créer un pense-bête et l’associer',
	'texte_donneur' => 'L’émetteur du pense-bête est l’auteur n°@id@.',
	'texte_nouveau_pensebete' => 'Nouveau pense-bête',
	'texte_objet_associe' => 'Le pense-bête a été associé à l’objet `@objet_associe@` n°@id@.',
	'texte_option_intro_receveur' => 'Sélectionnez un auteur',
	'texte_pble_creation' => 'Le pense-bête n’a pas pu être créé.',
	'texte_receveur_id' => 'L’auteur n°@id@ est receveur.',
	'texte_receveurs_ids' => '@nb@ auteurs n°@id@ sont receveurs.',
	'titre_a_publie' => 'A publié sur mon mur :',
	'titre_a_publie_sur_mon_mur' => '@nom@ a publié sur mon mur',
	'titre_activite_mur' => 'Activité du mur',
	'titre_jai_publie_sur_le_mur' => 'J’ai publié sur le mur de @nom@',
	'titre_jai_publie_sur_les_murs' => 'J’ai publié sur les murs de :',
	'titre_mur_autre' => 'le mur de @nom@',
	'titre_mur_mien' => 'sur mon mur',
	'titre_mur_particulier' => 'sur mon mur par @nom@',
	'titre_murs' => 'Les Pense-bêtes...',
	'titre_murs_autres' => 'sur les murs des autres',
	'titre_pensebete' => 'Pense-bête',
	'titre_pensebetes' => 'Pense-bêtes',
];
