<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/aide-pensebetes?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// A
	'assopb' => 'Associa note adesive',
	'assopb_association' => 'Quale promemoria associare?',
	'assopb_possibles' => 'Definizione di oggetti su cui posizionare Sticky Notes',

	// T
	'textepb' => 'Il contenuto del Promemoria',
	'textepb_conventions' => 'Convenzioni tipografiche',
	'textepb_taille' => 'Il numero di caratteri consentito',
];
