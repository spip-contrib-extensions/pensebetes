<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/pensebetes-paquet-xml-pensebetes?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// P
	'pensebetes_description' => 'Un plug-in destinato a ricordare cosa intendi fare o per ricordare a qualcuno cosa fare: promemoria da parete per la parte privata di SPIP.',
	'pensebetes_nom' => 'Pense-bêtes',
	'pensebetes_slogan' => 'Un plugin che cattura!',
];
