<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/pensebetes.git

return [

	// A
	'assopb' => 'Associer des Pense-bêtes',
	'assopb_association' => 'Quel Pense-bête associer ?',
	'assopb_possibles' => 'Définir les Objets sur lequels placer des Pense-bêtes',

	// T
	'textepb' => 'Le contenu du Pense-bête',
	'textepb_conventions' => 'Les conventions typographiques',
	'textepb_taille' => 'Le nombre de caractères autorisé',
];
