<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/aide-pensebetes?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// A
	'assopb' => 'Associate sticky notes',
	'assopb_association' => 'Which Reminder to associate?',
	'assopb_possibles' => 'Defining Objects on which to place Sticky Notes',

	// T
	'textepb' => 'The contents of the Reminder',
	'textepb_conventions' => 'Typographic conventions',
	'textepb_taille' => 'The number of characters allowed',
];
