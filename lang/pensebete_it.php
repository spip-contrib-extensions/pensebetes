<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/pensebete-pensebetes?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_boites' => 'Scatole informative',
	'cfg_boites_explication' => 'Caselle attività per promemoria inviati o ricevuti visualizzati lateralmente su:',
	'cfg_corps' => 'Numero di caratteri per il testo',
	'cfg_espace_public' => 'Foglio di stile nella parte pubblica',
	'cfg_explication_espace_privee' => 'Il promemoria è un mezzo di comunicazione tra gli autori, invisibile al pubblico. Sticky Notes compaiono sulla bacheca dell’autore, ma anche sugli oggetti a cui saranno collegati. Indicare di seguito le possibili connessioni',
	'cfg_explication_espace_public' => 'L’installazione del foglio di stile privato del plug-in nella parte pubblica ti consente di trarre vantaggio dall’aspetto delle note adesive come lo vedi nella parte privata (questa è solo un’opzione. Il tuo tema per lo spazio pubblico potrebbe fornire un altro aspetto.).',
	'cfg_height' => 'Altezza della nota adesiva',
	'cfg_la_class' => 'Definisci l’altezza e la larghezza della nota adesiva',
	'cfg_la_class_explication' => 'Le dimensioni contano, dicono. Ti permettiamo quindi di definire l’altezza e la larghezza della nota adesiva (i valori predefiniti durante l’installazione sono 12em e 12em)',
	'cfg_les_caracteres' => 'Definisci il numero di caratteri per la nota adesiva',
	'cfg_les_caracteres_explication' => 'Altri dicono che è il personaggio che conta. Ti permettiamo quindi di definire il numero di caratteri nelle note adesive (i valori di default sono 17 per il titolo e 110 per il corpo).',
	'cfg_lieux' => 'Note adesive in (luoghi):',
	'cfg_lieux_explication' => 'Mostra le note dell’autore sulle pagine:',
	'cfg_listes' => 'Elenco degli autori', # MODIF
	'cfg_listes_explication' => 'Consentono di pubblicare un promemoria sui muri di una selezione di autori', # MODIF
	'cfg_multiple' => 'Selezione multipla di autori', # MODIF
	'cfg_multiple_explication' => 'Consentono di mettere lo stesso promemoria sui muri di diversi autori',
	'cfg_objets' => 'Note adesive su (oggetti):',
	'cfg_public' => 'Spazio pubblico',
	'cfg_statuts' => 'Stato degli autori che possono scambiarsi promemoria',
	'cfg_statuts_explication' => 'Fai attenzione se convalidi i visitatori (6forum), poiché ciò richiede la gestione di note adesive nello spazio pubblico',
	'cfg_taille' => 'Le misure contano ...',
	'cfg_titre' => 'Numero di caratteri per il titolo',
	'cfg_width' => 'Larghezza della nota adesiva',

	// D
	'demo_espacepublic_off' => 'Attenzione, è necessario configurare il plugin per utilizzare il foglio di stile nell’area pubblica e vedere le note adesive.', # MODIF
	'demo_titre' => 'Dimostrazione di Pense-bêtes',

	// E
	'erreur_association' => 'Viene creato il promemoria (n°@id_pensebete@) ma non è stato possibile effettuare l’associazione con l’oggetto editoriale.',
	'erreur_suppression' => 'Non disponi dell’autorizzazione per eliminare questa nota adesiva',

	// I
	'icone_creer_pensebete' => 'Crea un promemoria',
	'icone_modifier_pensebete' => 'Modifica questo promemoria',
	'info_1_pensebete' => '1 promemoria',
	'info_1_pensebete_donne' => '1 promemoria dato',
	'info_1_pensebete_recu' => '1 promemoria ricevuto',
	'info_aucun_pensebete' => 'Nessun promemoria',
	'info_aucun_pensebete_donne' => 'Nessun promemoria fornito',
	'info_aucun_pensebete_recu' => 'Nessun promemoria ricevuto',
	'info_lassociation' => 'L’associazione',
	'info_le_pensebete' => 'Il promemoria',
	'info_nb_a_moi' => 'di cui @nb@ dei miei',
	'info_nb_par_moi' => 'di cui @nb@ da me',
	'info_nb_pensebetes' => '@nb@ solleciti',
	'info_nb_pensebetes_donnes' => '@nb@ promemoria dati',
	'info_nb_pensebetes_recus' => '@nb@ solleciti ricevuti',

	// L
	'label_a' => 'a',
	'label_de' => 'da',
	'label_donneur' => 'da',
	'label_listes' => 'Liste',
	'label_message' => 'Messaggio',
	'label_receveur' => 'Per',
	'label_titre' => 'Titolo',
	'lien_retirer_pensebete' => 'Rimuovi questo promemoria',
	'log_action_supprimer_pensebete' => 'action_supprimer_pensebete_dist : eliminazione da parte dell’autore n°@aut@ della nota adesiva n°@id@ dalla tabella "spip_pensebetes".',
	'log_action_supprimer_pensebete_imp' => 'action_supprimer_pensebete_dist : la cancellazione da parte dell’autore n°@aut@ della nota adesiva n°@id@ dalla tabella "spip_pensebetes" si è rivelata impossibile.',
	'log_action_supprimer_receveur' => 'action_supprimer_pensebete_dist : cancellazione da parte dell’autore n°@aut@ di @nb@ voci) nella tabella "spip_pensebetes_receveurs" per il promemoria n°@id@.',
	'log_action_supprimer_receveur_imp' => 'action_supprimer_pensebete_dist : la cancellazione da parte dell’autore n°@aut@ del record della tabella "spip_pensebetes_receveurs" per la nota adesiva n°@id@ si è rivelata impossibile.',

	// M
	'modele_bouton' => 'Pulsante',
	'modele_btn_texte' => 'Invia la tua risposta a @au@', # MODIF
	'modele_cocher' => 'casella di controllo',
	'modele_explication_q' => 'Domanda posta. Ricevuto in risposta.', # MODIF
	'modele_fait' => 'E ’fatto !',
	'modele_label_non' => 'No',
	'modele_label_oui' => 'Sì',
	'modele_pas_fait' => 'Non fatto',
	'modele_question' => 'Domanda',
	'modele_reponse_bnt' => 'Ok',
	'modele_reponse_succes' => 'La tua risposta è stata inviata con successo!',
	'modele_reponse_succes_titre' => 'successo',

	// S
	'saisies_obligatoire_receveur' => 'Il promemoria deve avere un destinatario.',

	// T
	'texte_ajouter_pensebete' => 'Aggiungi un promemoria',
	'texte_association' => 'Associazione',
	'texte_associer_pensebete' => 'Vuoi associare questa nota adesiva a questo oggetto?',
	'texte_avertissement_retrait' => 'Sei sicuro di voler eliminare questa nota adesiva?',
	'texte_creer_associer_pensebete' => 'Crea un promemoria e associalo',
	'texte_donneur' => 'Il mittente del sollecito è l’autore n°@id@.',
	'texte_nouveau_pensebete' => 'Nuovo promemoria',
	'texte_objet_associe' => 'Il promemoria è stato associato all’oggetto `@objet_associe@` n°@id@.',
	'texte_option_intro_receveur' => 'senza selezione', # MODIF
	'texte_pble_creation' => 'Impossibile creare la nota adesiva.',
	'texte_receveur_id' => 'L’autore n°@id@ è il destinatario.',
	'texte_receveurs_ids' => 'Gli @nb@ autori n°@id@ sono destinatari.',
	'titre_a_publie' => 'Postato sulla mia bacheca:',
	'titre_a_publie_sur_mon_mur' => '@nom@ postato sul mio muro',
	'titre_activite_mur' => 'Attività a parete',
	'titre_jai_publie_sur_le_mur' => 'Ho postato sul muro di @nom@',
	'titre_jai_publie_sur_les_murs' => 'Ho pubblicato sui muri di:',
	'titre_mur_autre' => 'il muro di @nom@',
	'titre_mur_mien' => 'sul mio muro',
	'titre_mur_particulier' => 'on my wall di @nom@',
	'titre_murs' => 'I promemoria ...',
	'titre_murs_autres' => 'sulle pareti di altre persone',
	'titre_pensebete' => 'Promemoria',
	'titre_pensebetes' => 'Solleciti',
];
