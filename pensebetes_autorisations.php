<?php
/**
 * Définit les autorisations du plugin Pensebetes
 *
 * @plugin     Pensebetes
 * @copyright  depuis 2019
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Pensebetes\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function pensebetes_autoriser() {
}

/**
 * Autorisation d'associer un pensebete
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/

function autoriser_associerpensebetes_dist($faire, $type, $id, $qui, $opt) {
	return true;
}


/**
 * Fonctions relatives aux menus de l'interface privée
 *
 */

/**
 * Autorisation de voir un élément de menu (pensebetes)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_pensebetes_menu_dist($faire, $type, $id, $qui, $opt) {
	 return true;
}


/**
 * Autorisation de voir le bouton d'accès rapide de création (pensebete)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_pensebetecreer_menu_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de voir le bouton d'outil collaboratif du mur
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_murs_menu_dist($faire, $type, $id, $qui, $opt) {
	return include_spip('inc/config') and in_array($qui['statut'], lire_config('pensebetes/mes_statuts', array()));
}

/**
 * Fonctions relatives à l'Objet pensebete
 *
 */

/**
 * Autorisation de créer (pensebete)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_pensebete_creer_dist($faire, $type, $id, $qui, $opt) {
	return include_spip('inc/config') and in_array($qui['statut'], lire_config('pensebetes/mes_statuts', array()));
}

/**
 * Autorisation de créer un pense-bête dont les destinataires seront les auteurs d'un certain statut
 *
 * @param string         $faire   Action demandée : `creer`
 * @param string         $type    Type d'objet sur lequel appliquer l'action : objet `_pensebete_par_statut`
 *                                A noter que les underscores sont retirés par l'appel de la fonction autoriser
 *                                et qu'il convient de débuter le nom de l'objet par un underscore pour
 *                                préciser que ce n'est pas un objet.
 * @param int            $id      Identifiant de l'objet : `0`, liste
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_pensebeteparstatut_creer_dist($faire, $type, $id, $qui, $opt) {
	if (
		// il faut que le plugin ait une configuration qui autorise cumulativement
		// - l'envoi à des auteurs multiples,
		// - l'envoi d’un pense-bête à tous les auteurs d’un certain statut
		autoriser('creer','_pensebete_multi_auteurs')
		and include_spip('inc/config')
		and lire_config('pensebetes/listes','') == 'on'
		// et que l’auteur soit lui-même d’un statut autorisé pour cette action
		// choix par défaut, seulement l'admin
		and in_array($qui['statut'], ['0minirezo'])
	){
		return true;
	}

	return false;
}

/**
 * Autorisation de créer un pense-bête dont les destinataires peuvent être multiples
 *
 * @param string         $faire   Action demandée : `creer`
 * @param string         $type    Type d'objet sur lequel appliquer l'action : objet `_pensebete_multi_auteurs`
 *                                A noter que les underscores sont retirés par l'appel de la fonction autoriser
 *                                et qu'il convient de débuter le nom de l'objet par un underscore pour
 *                                préciser que ce n'est pas un objet.
 * @param int            $id      Identifiant de l'objet : `0`, liste
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $options Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_pensebetemultiauteurs_creer_dist($faire, $type, $id, $qui, $opt) {
	return
		// il faut que le plugin ait une configuration qui autorise 
		// l'envoi d’un pense-bête à tous les auteurs d’un certain statut
		include_spip('inc/config')
		and lire_config('pensebetes/multiple','') == 'on'
		// et que l’auteur soit lui-même d’un statut autorisé pour cette action
		// ici seulement les admin et les rédac
		and in_array($qui['statut'], ['0minirezo', '1comite']);
}

/**
 * Autorisation de voir (pensebete)
 *
 * On peut voir les pensebetes dont on est l'auteur ou le destinataire.
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_pensebete_voir_dist($faire, $type, $id, $qui, $opt) {

	# il faut être un auteur pour avoir une autorisation, et celle-ci est systématiquement donnée à l'administrateur
	if (!$id_auteur = intval($qui['id_auteur'])) {
		return false;
	} elseif ($qui['statut'] == '0minirezo') {
		return true;
	}

	# demande d'une liste ou d'un pensebete ?
	$id_pensebete = (int) $id;
	if (!$id_pensebete){
		return false;
	}

	# entre quels auteurs peuvent s'échanger des Penses-bêtes ?
	include_spip('inc/config');
	$mes_statuts = lire_config("pensebetes/mes_statuts",['0minirezo', '1comite']);
	if (!in_array($qui['statut'], $mes_statuts)){
		return false;
	}

	# savoir qui le donneur du pense-bête
	if (!isset($opt['id_donneur']) or !$id_donneur = (int) $opt['id_donneur']){
		$id_donneur = sql_getfetsel('id_donneur', 'spip_pensebetes', 'id_pensebete=' . $id_pensebete);
	}

	# on peut voir le pensebete que l'on a donné
	if ($qui['id_auteur'] == $id_donneur) {
		return true;
	}

	# savoir qui est le receveur du pense-bête
	$receveurs = array();
	if (!isset($opt['id_receveur']) or !$id_receveur = intval($opt['id_receveur'])){
		if ($r = sql_select('id_receveur', 'spip_pensebetes_receveurs', 'id_pensebete='. $id_pensebete)) {
			while ($ligne = sql_fetch($r)) {
				$receveurs[] = $ligne['id_receveur'];
			}
		}
	}
	# on peut voir un pensebete dont on est le receveur
	if (in_array($qui['id_auteur'], $receveurs)) {
		return true;
	}

	return false;
}

/**
 * Autorisation de modifier (pensebete)
 *
 * On peut modifier que les pensebetes dont on est l'auteur.
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_pensebete_modifier_dist($faire, $type, $id, $qui, $opt) {

	// l'auteur doit détenir le statut adhoc pour modifier un pense-bête
	if (in_array($qui['statut'], lire_config('pensebetes/mes_statuts', array()))
		// et en être le donneur
		and (
				(isset($opt['id_donneur']) and $opt['id_donneur'] === $qui['id_auteur'])
					or 
				($id = (int) $id and $qui['id_auteur'] == sql_getfetsel('id_donneur', 'spip_pensebetes', 'id_pensebete=' . $id))
					or 
		// ou être l'admin
				($qui['statut'] == '0minirezo' and !$qui['restreint'])
			)
	){
		return true;
	}

	return false;
}

/**
 * Autorisation de supprimer (pensebete)
 *
 * On peut supprimer tous les pensebetes que l'on peut voir.
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_pensebete_supprimer_dist($faire, $type, $id, $qui, $opt) {
	return autoriser_pensebete_voir_dist($faire, $type, $id, $qui, $opt);
}

/**
 * Autorisation d'iconifier un pense-bête,
 * c'est à dire lui mettre un logo.
 *
 * @param  string $faire Action demandée
 * @param  string $type Type d'objet sur lequel appliquer l'action
 * @param  int $id Identifiant de l'objet
 * @param  array $qui Description de l'auteur demandant l'autorisation
 * @param  array $opt Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_pensebete_iconifier_dist($faire, $type, $id, $qui, $opt) {
	// totalement inutile
	return false;
}
