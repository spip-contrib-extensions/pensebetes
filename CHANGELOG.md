# Changelog du plugin Pensebetes.
- Tenir un changelog : https://www.spip.net/fr_article6825.html
- Ecrire un commit : https://www.spip.net/fr_article6824.html

## [Unreleased]

### Changed
Commit b25b552b :
- Mise en conformité des fichiers de langue. Adopter la syntaxe valide à partir de SPIP 4.1 ce qui facilitera la migration en SPIP 5. 
- PSR

## [2.2.0]

### Changed
- Fichier `../paquet.xml` bornes de compatibilité SPIP aux versions maintenues.
- Fichier `../formulaires/configurer_pensebetes.php` Subordonner l'envoi à une catégorie d'auteurs à l'acceptation d'un envoi à des auteurs multiples.
- Fichier `../pensebetes/pensebetes_autorisations.php` Subordonner l'autorisation d'envoi à une catégorie d'auteurs à l'acceptation d'un envoi à des auteurs multiples.

### Fixed
- Fichier `../formulaires/editer_pensebete.php` Lors de la modification d'un pense-bête, le formulaire d'édition ne sélectionnait pas, par défaut, l'auteur précédement selectionné si le mode *simple* était activé. En mode *multiple*, pas de bug.
- Fichier `../pensebetes_autorisations.php` warming.

## [2.1.3]

### Fixed
- Fichier : `../pensebetes/prive/squelettes/inclure/pensebetes_affiche.html` issue8 + non utilisation modele_de_pagination
- Fichier : `../pensebetes/prive/squelettes/inclure/pensebetes_affiche_recus.html` issue8 + non utilisation modele_de_pagination


## [2.1.2]

### Added
- Fichier : `../pensebetes/prive/themes/spip/images/enlever-12.svg` contourne le problème d'affichage en vignette avec un svg non redimentionné.
- Fichier : `../pensebetes/modeles/pensebete.html` ajoute la gestion d'un id (issue9)

## [2.1.1]

### Fixed
- Fichier : `../pensebetes/formulaires/editer_pensebete.php` utilisation dépréciée de strlen avec null
- Fichier `../paquet.xml` : utilise Yaml v3.0.3 sinon Saisies 4.7.1 fait des caprices.
- Fichier `../pensebetes/action/repondre_pensebete.php` coche à la place de a_faire

## [2.1.0]

Simplification drastique des MODELEs.
Amélioration de l'ergonomie du formulaire de création de pense-bête.
Optimisation de la base de données

### Removed
- Fichier : `pbq-xx.svg` Supression pour d'autres icones différenciant la prestation proposée.

### Changed
- Fichier : `../pensebetes/modeles/pbq.html` Simplification.
- Fichier : `../pensebetes/lang/pensebete_fr.php` Simplification.
- Fichier : `../pensebetes/formulaires/editer_pensebete.php` Clarification du choix entre les auteurs et une catégorie d'auteurs.
- Fichier : `../pensebetes/pensebetes_pipelines.php` Optimisation de la base de données avec la fonction `pensebetes_optimiser_base_disparus()`.

# Added
- Fichier : `../pensebetes/formulaires/traiter_pbq.php` Fonction identifiant à qui répondre. 
- Fichiers : `pbcocher-xx.svg`, `pbouinon-xx.svg` Icône pour oui_non et cocher.

# Fixed
- Fichier : `../pensebetes/formulaires/editer_pensebete.php` mauvaise utilisation de la fonction defined (issue7).

## [2.0.9]

### Changed
- modification : `mur-xx.svg`

### Removed
- retrait : `mur_voir-xx.svg`

## [2.0.8]

- utilise le *plugin select2* pour clarifier et simplifier la selection d'auteurs nombreux.

### Added
- Fichier : `../pensebetes_autorisations.php`
  création de deux autorisations :
  o  `autoriseer('creer', '_pensebete_multi_auteurs')`
  o  `autoriseer('creer', '_pensebete_par_statut')`

### Fixed
- Fichier : `../formulaires/editer_pensebete.php`
  o  le double choix entre un auteur spécifique et d'une liste d'auteurs ne permettait pas de choisir l'une ou l'autre des propositions.
  o  cas spécifique d'un appel en modification depuis une liste suivi d'un retour en édition suite à une erreur.
- Fichier : `../prive/squelettes/inclure/pensebetes_donnes.html` simplification d'une écriture inutilement complexe d'un idiome de langue.
- Fichier : `../prive/squelettes/inclure/pensebetes_recus.html` simplification d'une écriture inutilement complexe d'un idiome de langue.
- Fichier : `../lang/pensebete_fr.php`toilettage avec l'aide du plugin langOnet des idiomes obsolètes.

## 2.0.7

La version 2.0.7
- assure la compatibilité avec SPIP 4.2.* 
- utilise Salvatore pour mieux prendre en charge les traductions,
- ajoute une nouvelle fonction, celle de pouvoir poser une question par le pense-bete et obtenir une réponse par le même moyen.
- propose une nouvelle fonction pour pouvoir envoyer sans identification un pensebete à un auteur pour lui signaler, par exemple, la fin d'une tâche.

### Added
- Fichier `../README.md`
- Fichier `../CHANGELOG.md`
- Fichier `../inc/envoyer_pensebete.php` : fonction pour créer un pensebete sans identification.
- Fichier `../modeles/pdq.html` : modèle permettant d'intégrer une question dans le pense-bête.
- Fichier `../action/repondre_pensebete.php`: action qui permet d'envoyer les réponses à la question par un pense-bête.
- Fichier `../modeles/pdq.yaml` : description utilisée par le plugin `inserer_modeles`
- Modification `../paquet.xml` : utilise le plugin `inserer_modeles` v2.0.2 Pensez à activer le plugin pour les Pense-bêtes dans la configuration.

### Changed
- Modification `../css/style_plugin_pensebetes.css.html` : la propriété `background` de la classe `monpostit` est supprimée afin que le pense-bête soit *poser dessus* sans modifier *l'aspect du dessous*, un comportement qui est attendu.

### Fixed
- Modification `../pensebetes_autorisations` : autoriser_pensebete_voir_dist() correction d'un warning.
- Modification `../base/pensebetes.php` : déclaration du `type` de la table spip_pensebetes.